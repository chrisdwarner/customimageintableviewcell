//
//  AppDelegate.h
//  customImageInTableViewCell
//
//  Created by chris warner on 11/29/15.
//  Copyright © 2015 ChrisDWarner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

