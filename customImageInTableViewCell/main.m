//
//  main.m
//  customImageInTableViewCell
//
//  Created by chris warner on 11/29/15.
//  Copyright © 2015 ChrisDWarner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
