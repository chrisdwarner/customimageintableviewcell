//
//  CustomTableViewCell.m
//  customImageInTableViewCell
//
//  Created by chris warner on 11/29/15.
//  Copyright © 2015 ChrisDWarner. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell
@synthesize icon;


-(instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        // create the icon and add it to the contentView.
        self.icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon.jpg" inBundle:nil compatibleWithTraitCollection:nil]];
        [self.contentView addSubview:self.icon];

        // Now we have added an icon image to the tableview cell.  Problem is that we have not configured that image to fit within the cell.
        // We will use autolayout to get the image to fit within the UITableViewCell.
        [self.icon setTranslatesAutoresizingMaskIntoConstraints:NO];

        // setup the height and width of the icon
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[icon(24)]"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"icon":self.icon}]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[icon(24)]"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"icon":self.icon}]];

        // Now we position the icon on the right side of the tableviewCell in such a way that it is compatible with the universal layout

        // center the icon horizontally within the UITableViewCells contentview.
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.icon
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0
                                                                      constant:0.0]];

        // Now attach the icon to the right hand margin.
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.icon
                                                                     attribute:NSLayoutAttributeTrailing
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeTrailingMargin
                                                                    multiplier:1.0
                                                                      constant:0.0]];

    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// Make sure that we apply the layout rules on the tableview cell
-(void) layoutSubviews
{
    [super layoutSubviews];

    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}
@end
