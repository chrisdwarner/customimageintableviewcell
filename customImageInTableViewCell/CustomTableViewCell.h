//
//  CustomTableViewCell.h
//  customImageInTableViewCell
//
//  Created by chris warner on 11/29/15.
//  Copyright © 2015 ChrisDWarner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *icon;

@end
